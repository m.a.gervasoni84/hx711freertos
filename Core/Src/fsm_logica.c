#include "main.h"
#include "gpio.h"
#include "lcd.h"
#include "fsm_logica.h"
#include "queue.h"
#include <string.h>


QueueHandle_t queue_lcd_send;

queue_send_item_t item;




MENU_ESTADO estado_menu;


void fsm_init(void) {
	xTaskCreate(fsm_menu, (const char*) "fsm_menu", configMINIMAL_STACK_SIZE, NULL, osPriorityNormal, NULL);

	queue_lcd_send = xQueueCreate(QUEUE_SEND_LEN, sizeof(queue_send_item_t));

	estado_menu = OPCION_1;
}


void fsm_menu(void *pvP) {

	while (1) {

		char *aux;
		//uint8_t mensaje[men_LEN] = {0}; //para los datos
		switch (estado_menu) {
		case OPCION_1:
			aux = "Con Queue!!";
			strncpy(item.mensaje, aux, strlen(aux));
			item.estado_Lcd = SEND_DATA;
			item.row = 0;
			item.col = 0;
			xQueueSend(queue_lcd_send,&item,0);

			vTaskDelay(2000);

			aux = "Otro Queue!";
			strncpy(item.mensaje, aux, strlen(aux));
			item.estado_Lcd = SEND_DATA;
			item.row = 0;
			item.col = 0;
			xQueueSend(queue_lcd_send,&item,0);

			estado_menu = OPCION_3;
			vTaskDelay(2000);
			break;

		case OPCION_2:
//			lcd_send_string("opcion 2");
			break;
		case OPCION_3:
			aux = "clear";
			strncpy(item.mensaje, aux, strlen(aux));
			item.estado_Lcd = CLEAR;
			item.row = 0;
			item.col = 0;
			xQueueSend(queue_lcd_send,&item,0);
			vTaskDelay(2000);
			estado_menu = OPCION_1;
			break;
		case OPCION_4:
//			lcd_send_string("opcion 4");
			break;
		}
		vTaskDelay(4000);
		taskYIELD();
	}

}


