#define _GNU_SOURCE
#include "main.h"
#include "uart.h"
#include "hx711.h"

#include <stdio.h>

#define QUEUE_UART_ITEM_COUNT 5

static QueueHandle_t queue_uart;

void uartTaskInit() {

	xTaskCreate(task1, (const char*) "task1", configMINIMAL_STACK_SIZE, NULL,	osPriorityNormal, NULL);
	xTaskCreate(task2, (const char*) "task2", configMINIMAL_STACK_SIZE, NULL,	osPriorityNormal, NULL);

	queue_uart = xQueueCreate(QUEUE_UART_ITEM_COUNT, 4);
}

void task1(void *a) {
	uint32_t x = 0;
	while (1) {
		x++;
		xQueueSend(queue_uart, &x, portMAX_DELAY);
		vTaskDelay(500);
	}

}

void task2(void *a) {
	uint32_t y = 0;
	while (1) {

		xQueueReceive(queue_uart, &y, portMAX_DELAY);
		printf("y = %lu \r \n", y);
		vTaskDelay(1000);
	}

}
