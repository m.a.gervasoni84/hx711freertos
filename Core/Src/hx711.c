//
//#define _GNU_SOURCE
//#include "main.h"
//#include "hx711.h"
//#include <stdio.h>
//
//static QueueHandle_t Queue_HX711;
//
//void hx711TaskInit() {
//	Queue_HX711 = xQueueCreate(1, sizeof(uint32_t));
//	xTaskCreate(hx711Task, (const char*) "hx711Task", configMINIMAL_STACK_SIZE,
//			NULL, osPriorityNormal, NULL);
//}
//
//void hx711Task(void *pvParameters) {
//	uint32_t buffer = 0;
//	uint32_t dato = 0;
//	while (1) {
//		taskENTER_CRITICAL();
//		for (int i = 0; i < PROM; i++) {
//			buffer = HX711_leer();
//			dato += buffer;
//		}
//		taskEXIT_CRITICAL();
//
//		dato = dato/PROM; // 5 datos para hacer mas estable la medicion
//
//		xQueueSend(Queue_HX711, &dato, portMAX_DELAY);
//	}
//	printf("valor: %ld", dato);
//	vTaskDelay(10 / portTICK_PERIOD_MS); //cada 10ms
//}
//
//uint32_t HX711_leer() {
//	uint32_t buffer;
//	buffer = 0;
//
//	HAL_GPIO_WritePin(HX711_SCK_GPIO_Port, HX711_SCK_Pin, GPIO_PIN_RESET);
//
//	while (HAL_GPIO_ReadPin(HX711_DAT_GPIO_Port, HX711_DAT_Pin) == GPIO_PIN_SET)
//		;
//
//	HX711_wait100ns(2);
//
//	for (uint8_t i = 0; i < 24; i++) {
//		HAL_GPIO_WritePin(HX711_SCK_GPIO_Port, HX711_SCK_Pin, GPIO_PIN_SET);
//
//		HX711_wait100ns(3);
//
//		buffer = buffer << 1;
//		if (HAL_GPIO_ReadPin(HX711_DAT_GPIO_Port, HX711_DAT_Pin)) {
//			buffer++;
//		}
//
//		HAL_GPIO_WritePin(HX711_SCK_GPIO_Port, HX711_SCK_Pin, GPIO_PIN_RESET);
//		HX711_wait100ns(3);
//	}
//
//	for (uint8_t i = 0; i < 1; i++) {
//		HAL_GPIO_WritePin(HX711_SCK_GPIO_Port, HX711_SCK_Pin, GPIO_PIN_SET);
//		HX711_wait100ns(3);
//		HAL_GPIO_WritePin(HX711_SCK_GPIO_Port, HX711_SCK_Pin, GPIO_PIN_RESET);
//		HX711_wait100ns(3);
//	}
//
//	buffer = buffer ^ 0x800000;
//
//	return buffer;
//}
//
////Revisar bien esta funcion
//void HX711_wait100ns(uint32_t ns_100) {
//	TIM1->ARR = ns_100;
//	TIM1->EGR |= TIM_EGR_UG;			//genera un evento de actualización del temporizador
//	TIM1->SR &= ~TIM_SR_UIF;			//borra la bandera de interrupción de desbordamiento del temporizador
//	TIM1->CCR1 |= TIM_CR1_CEN;			//inicia la captura del canal 1 del temporizador TIM1 al habilitar el contador
//	while (!(TIM1->SR & TIM_SR_UIF)); 	//bucle espera hasta que la bandera de interrupción de desbordamiento del temporizador se establezca, lo que indica que el temporizador ha terminado de contar.
//	TIM1->CCR1 &= ~TIM_CR1_CEN;			//Esto detiene el contador del canal 1 del temporizador TIM1 al deshabilitar el contador (CEN)
//	TIM1->SR &= ~TIM_SR_UIF;			//borra nuevamente la bandera de desbordamiento del temporizador
//	HAL_GPIO_WritePin(HX711_SCK_GPIO_Port, HX711_SCK_Pin, GPIO_PIN_SET);
//
//}
