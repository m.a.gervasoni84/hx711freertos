/*


 */

/*==================[inclusions]=============================================*/

#include "main.h"
#include "gpio.h"
#include "lcd.h"
#include "fsm_logica.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/*==================[macros and definitions]=================================*/

#define SLAVE_ADDRESS_LCD 0x4E //(01001110) direccion por default del PCF8574 con los pines A0,A1,A2 de la placa se cambia
/*Tambien puede ser 0x7E, entonces sera el caso de un PCF8574A*/

QueueHandle_t queue_lcd_get;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

LCD_I2C_STATES estado_Lcd;

queue_send_item_t item_recibido;

static bool e_Tick1_ms;

static bool send_data_ok = 1;
static bool put_cur_ok;
static bool clear_ok;

static char str_buff[STR_LEN];
//static char user_buff[STR_LEN];
size_t lcd_len;
static uint8_t data_t[CMD_BUFF_LEN];
static uint8_t data2_t[DATA_BUFF_LEN];
static char *puntero_str;
static char put_cur;
int i;

static uint8_t LEN;

/** @brief tick counter */
static uint32_t countLcd_ms;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** @brief This function handles Hard fault interrupt.  */

void lcd_send_cmd(char cmd) {
	char data_u, data_l;
	data_u = (cmd & 0xf0);
	data_l = (cmd << 4);
	data_t[0] = data_u | 0x0C;
	data_t[1] = data_u | 0x08;
	data_t[2] = data_l | 0x0C;
	data_t[3] = data_l | 0x08;

	HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);

}

void lcd_send_data(char data) {
	char data_u, data_l;
	data_u = (data & 0xf0);
	data_l = (data << 4);
	data2_t[0] = data_u | 0x0D;
	data2_t[1] = data_u | 0x09;
	data2_t[2] = data_l | 0x0D;
	data2_t[3] = data_l | 0x09;
}

static void clearEvents(void) {
	e_Tick1_ms = 0;
}

/*==================[external functions definition]==========================*/

void lcd_init(void) {

	xTaskCreate(lcd_task, (const char*) "lcd_task", configMINIMAL_STACK_SIZE,
	NULL, osPriorityNormal+1, NULL);
	queue_lcd_get = xQueueCreate(QUEUE_SEND_LEN, sizeof(queue_send_item_t));

	countLcd_ms = 0;
	send_data_ok = 0;
	put_cur_ok = 0;
	clear_ok = 0;
	bzero(data_t, CMD_BUFF_LEN);
	bzero(data2_t, CMD_BUFF_LEN);
	bzero(str_buff, STR_LEN);
	i = 0;
	LEN = 0;
	estado_Lcd = ESTADO1;
}

void lcd_task(void *pv) {
	while (1) {
		switch (estado_Lcd) {
		case ESTADO1:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT1)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT1)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x30);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO2;
			}
			break;

		case ESTADO2:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT2)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT2)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x30);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO3;
			}
			break;

		case ESTADO3:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT3)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT3)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x30);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO4;
			}
			break;

		case ESTADO4:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT4)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT4)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x20);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO5;
			}
			break;

		case ESTADO5:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT5)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT5)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x28);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO6;
			}
			break;

		case ESTADO6:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT6)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT6)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x08);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO7;
			}
			break;

		case ESTADO7:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT7)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT7)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x01);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO8;
			}
			break;

		case ESTADO8:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT8)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT8)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x06);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = ESTADO9;
			}
			break;

		case ESTADO9:
			if (e_Tick1_ms && (countLcd_ms < TIME_BIT9)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_BIT9)) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x0C);
				lcd_send_cmd(0x0E);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				countLcd_ms = 0;
				estado_Lcd = IDLE_LCD;
			}

			break;

		case IDLE_LCD:
			if (e_Tick1_ms && (countLcd_ms < TIME_IDLE)) {
				countLcd_ms++;
				if (xQueueReceive(queue_lcd_send, &item_recibido, 0) == pdPASS) {
					if (item_recibido.estado_Lcd == SEND_DATA) {
						xSemaphoreTake(mutex_lcd, 0);
						LEN = strlen(item_recibido.mensaje);
						puntero_str = item_recibido.mensaje;
						lcd_put_cur(item_recibido.row, item_recibido.col);
						countLcd_ms = 0;
					}

					else if (item_recibido.estado_Lcd == CLEAR) { //Ver por que el comando no limpia la pantalla
						xSemaphoreTake(mutex_lcd, 0);
						estado_Lcd = CLEAR;
						clear_ok = 1;
						countLcd_ms = 0;
					}

				}
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_IDLE)) {
				countLcd_ms = 0;
			}

			break;

		case SEND_DATA:
			if (send_data_ok) {
				if (*puntero_str != '\0' && i <= LEN) {
					bzero(data2_t, DATA_BUFF_LEN);
					lcd_send_data(*puntero_str);
					HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data2_t, 4);
					send_data_ok = 0;
					puntero_str++;
					i++;
				}
			} else if (e_Tick1_ms && (countLcd_ms < TIME_SEND)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (*puntero_str) && (countLcd_ms >= TIME_SEND)) {
				countLcd_ms = 0;
				send_data_ok = 1;
			} else if (e_Tick1_ms && !(*puntero_str) && (countLcd_ms >= TIME_SEND)) {
				countLcd_ms = 0;
				estado_Lcd = IDLE_LCD;
				send_data_ok = 0;
				LEN = 0;
				i = 0;
				xSemaphoreGive(mutex_lcd);
			}

			break;

		case PUT_CUR:
			if (put_cur_ok) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(put_cur);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				put_cur_ok = 0;
			} else if (e_Tick1_ms && (countLcd_ms < TIME_PUT_CUR)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_PUT_CUR)) {
				countLcd_ms = 0;
				estado_Lcd = SEND_DATA;
			}
			break;

		case CLEAR:
			if (clear_ok) {
				bzero(data_t, CMD_BUFF_LEN);
				lcd_send_cmd(0x01);
				HAL_I2C_Master_Transmit_IT(&hi2c1, SLAVE_ADDRESS_LCD, (uint8_t*) data_t, 4);
				clear_ok = 0;
			} else if (e_Tick1_ms && (countLcd_ms < TIME_CLEAR)) {
				countLcd_ms++;
			} else if (e_Tick1_ms && (countLcd_ms >= TIME_CLEAR)) {
				countLcd_ms = 0;
				clear_ok = 0;
				estado_Lcd = IDLE_LCD;
			}
			break;

		}
		clearEvents();
		taskYIELD();
	}
}

void lcd_put_cur(int row, int col) {
	if (estado_Lcd == IDLE_LCD) {
		estado_Lcd = PUT_CUR;
		put_cur_ok = 1;
		countLcd_ms = 0;
		switch (row) {
		case 0:
			col |= 0x80;
			break;
		case 1:
			col |= 0x40;
			break;
		}
		put_cur = col;
	}
}

void lcd_tick(void) {
	e_Tick1_ms = 1;
}
/*==================[end of file]============================================*/
