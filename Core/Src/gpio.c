
#include "main.h"
#include "gpio.h"
#include "lcd.h"
#include "fsm_logica.h"

SemaphoreHandle_t semaphore_GPIO_UP;
SemaphoreHandle_t semaphore_GPIO_DOWN;
SemaphoreHandle_t semaphore_GPIO_OK;



static estado_pulsador estado_pulsador_id;


void gpio_init(){
	xTaskCreate(pulsadores_task, (const char*) "pulsadores_task", configMINIMAL_STACK_SIZE, NULL, osPriorityNormal, NULL);
}




void HAL_GPIO_EXTI_Callback(uint16_t GPIO_PIN) {
	static BaseType_t xHigherPriorityTaskWoken;

	switch (GPIO_PIN) {
	case UP_Pin:
//		estado_pulsador_id = UP;
		xSemaphoreGiveFromISR(semaphore_GPIO_UP, &xHigherPriorityTaskWoken);
		break;
	case DOWN_Pin:
//		estado_pulsador_id = DOWN;
		xSemaphoreGiveFromISR(semaphore_GPIO_DOWN, &xHigherPriorityTaskWoken);
		break;
	case MENU_OK_Pin:
//		estado_pulsador_id = OK;
		xSemaphoreGiveFromISR(semaphore_GPIO_OK, &xHigherPriorityTaskWoken);
		break;
	}
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}


void pulsadores_task(void *pv) {
	while (1) {

		if (xSemaphoreTake(semaphore_GPIO_UP, 0) == pdTRUE) {
			HAL_GPIO_WritePin(LED_BP_GPIO_Port, LED_BP_Pin, GPIO_PIN_RESET);
			estado_menu = OPCION_1;
		}
		if (xSemaphoreTake(semaphore_GPIO_DOWN, 0) == pdTRUE) {
			HAL_GPIO_WritePin(LED_BP_GPIO_Port, LED_BP_Pin, RESET);
			estado_menu = OPCION_2;
		}
		if (xSemaphoreTake(semaphore_GPIO_OK, 0) == pdTRUE) {
			estado_menu = OPCION_3;
		}
		taskYIELD();
	}

}



