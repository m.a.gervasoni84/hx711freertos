
#ifndef INC_GPIO_H_
#define INC_GPIO_H_
typedef enum{
	UP,
	DOWN,
	OK
}estado_pulsador;

extern SemaphoreHandle_t semaphore_GPIO_UP;
extern SemaphoreHandle_t semaphore_GPIO_DOWN;
extern SemaphoreHandle_t semaphore_GPIO_OK;


void gpio_init(void);

void pulsadores_task(void *pv);

#endif /* INC_GPIO_H_ */
