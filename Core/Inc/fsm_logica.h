

#ifndef INC_FSM_LOGICA_H_
#define INC_FSM_LOGICA_H_

#define QUEUE_SEND_LEN 10 // tamaño buffer enviar
#define QUEUE_CHAR_LEN 16 // largo maximo pantalla

typedef enum{
	OPCION_1, 	//menu
	OPCION_2,	//
	OPCION_3,	//configuracion
	OPCION_4	//inicio
}MENU_ESTADO;

extern MENU_ESTADO estado_menu;

//estructura para el dato a enviar a el lcd
typedef struct{
	char mensaje[QUEUE_CHAR_LEN];	//mensaje
	LCD_I2C_STATES estado_Lcd;			// funcion a realizar
	uint8_t row;	//Fila donde enviarlo
	uint8_t col;	//Columna donde enviarlo
}queue_send_item_t;


extern QueueHandle_t queue_lcd_send;
//extern queue_send_item_t item;










void fsm_init(void);

void fsm_menu(void *pvP);

#endif /* INC_FSM_LOGICA_H_ */
