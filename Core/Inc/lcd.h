
#ifndef INC_LCD_H_
#define INC_LCD_H_

#define SLAVE_ADDRESS_LCD 0x4E
#define men_LEN 20 // Largo maximo que entra en una linea de pantalla




#define TIME_BIT1 1
#define TIME_BIT2 5
#define TIME_BIT3 2
#define TIME_BIT4 10
#define TIME_BIT5 10
#define TIME_BIT6 1
#define TIME_BIT7 1
#define TIME_BIT8 2
#define TIME_BIT9 1
#define TIME_IDLE 10
#define TIME_SEND 50 // Velocidad con que aparece en pantalla
#define TIME_PUT_CUR 10
#define TIME_CLEAR 2
#define CMD_BUFF_LEN 4
#define DATA_BUFF_LEN 4
#define STR_LEN 20



typedef enum {
	ESTADO1,
	ESTADO2,
	ESTADO3,
	ESTADO4,
	ESTADO5,
	ESTADO6,
	ESTADO7,
	ESTADO8,
	ESTADO9,
	IDLE_LCD,
	SEND_DATA,
	PUT_CUR,
	CLEAR
}LCD_I2C_STATES ;






void lcd_init (void);   // inicializar lcd

void lcd_init_task(void *pv);

void lcd_task(void *var);

void lcd_send_cmd (char cmd);  //  commando al lcd

void lcd_send_data (char data);  // data al lcd

void lcd_menu(); //Funcion que controlaria el menu

//void lcd_send_string (char *str);  // string al lcd

void lcd_put_cur(int row, int col);  // colocar cursor fila/row (0 or 1), columna/col (0-15);


void lcd_tick(void);

#endif /* INC_LCD_H_ */
